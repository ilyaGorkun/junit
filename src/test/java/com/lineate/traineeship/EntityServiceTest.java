package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;

public class EntityServiceTest {


    @Test
    public void testCreateEntity() {
        UserService userService = new ServiceFactory().createUserService();

        EntityService entityService = new ServiceFactory().createEntityService();

        Group group = userService.createGroup("group",  Collections.singleton(Permission.read));

        User user = userService.createUser("user", group);

        Assert.assertTrue(entityService.createEntity(user, "entity", "value"));
    }


    @Test
    public void testGetValueEntity1(){
        UserService userService = new ServiceFactory().createUserService();

        EntityService entityService = new ServiceFactory().createEntityService();

        Group group = userService.createGroup("group", Collections.singleton(Permission.read));

        User user = userService.createUser("user", group);

        entityService.createEntity(user, "entity", "value");

        Assert.assertEquals("value",entityService.getEntityValue(user, "entity"));
    }


    @Test
    public void testGetValueEntity2(){
        UserService userService = new ServiceFactory().createUserService();

        EntityService entityService = new ServiceFactory().createEntityService();

        Group group = userService.createGroup("group", Collections.singleton(Permission.read));

        User user = userService.createUser("user", group);

        entityService.createEntity(user, "entity", "value");

        User user1 = userService.createUser("user1", group);

        Assert.assertNull(entityService.getEntityValue(user1, "entity"));
    }




    @Test
    public void testUpdateEntity(){
        UserService userService = new ServiceFactory().createUserService();

        EntityService entityService = new ServiceFactory().createEntityService();

        Group group = userService.createGroup("group", Collections.singleton(Permission.write));

        User user = userService.createUser("user", group);

        entityService.createEntity(user,"entity", "v1");

        Assert.assertTrue(entityService.updateEntity(user, "entity", "v2"));

        User user1 = userService.createUser("user1",group);

        Assert.assertTrue(entityService.updateEntity(user1, "entity", "value1"));

        Group group1 = userService.createGroup("group1",Collections.singleton(Permission.write));

        User user3 = userService.createUser("user3", group1);

        Assert.assertFalse(entityService.updateEntity(user3, "entity", "value1"));
    }



    @Test
    public void testEqualityOfGroupsUserAndEntity() {

        ServiceFactory serviceFactory = new ServiceFactory();

        UserService userService = serviceFactory.createUserService();

        EntityRepository entityRepository = Mockito.mock(EntityRepository.class);

        EntityService entityService = serviceFactory.createEntityService(entityRepository);

        Group group1 = userService.createGroup("group1", Collections.singleton(Permission.write));

        Group group2 = userService.createGroup("group2", Collections.singleton(Permission.read));

        Group group3 = userService.createGroup("group3", Collections.singleton(Permission.write));

        User user = userService.createUser("user", group1);

        user.getGroups().add(group2);

        user.getGroups().add(group3);

        entityService.createEntity(user, "entity", "value");

        Entity entity = new Entity(user, "entity");

        Mockito.when(entityRepository.get("entity")).thenReturn(entity);

        Assert.assertEquals(entityService.getEntity("entity").getGroups(),user.getGroups() );

    }

    @Test
    public void testAccessCheck() {
        ServiceFactory serviceFactory = new ServiceFactory();

        UserService userService = serviceFactory.createUserService();

        EntityService entityService = serviceFactory.createEntityService();

        Group group1 = userService.createGroup("group1", Collections.singleton(Permission.read));

        User user1 = userService.createUser("user1", group1);

        entityService.createEntity(user1, "entity1", "value");

        Assert.assertEquals("value", entityService.getEntityValue(user1, "entity1"));

        Group group2 = userService.createGroup("group2", Collections.singleton(Permission.write));

        User user2 = userService.createUser("user2", group2);

        entityService.createEntity(user2, "entity2", "value");

        user1.getGroups().add(group2);

        Assert.assertTrue(entityService.updateEntity(user1, "entity2", "value1"));

        Assert.assertEquals("value1", entityService.getEntityValue(user2, "entity2"));

    }
    

    @Ignore
    //Вот тут баг, число групп у сущностей не фиксируется
    public void testFixedNumberGroupsInEntity() {
        ServiceFactory serviceFactory = new ServiceFactory();

        UserService userService = serviceFactory.createUserService();

        EntityRepository entityRepository = Mockito.mock(EntityRepository.class);

        Group group1 = userService.createGroup("group1", Collections.singleton(Permission.read));

        User user1  = userService.createUser("user1", group1 );

        Entity entity = new Entity(user1, "entity");

        Group group2 = userService.createGroup("group2", Collections.singleton(Permission.read));

        user1.getGroups().add(group2);

        Mockito.when(entityRepository.get("entity")).thenReturn(entity);

        Assert.assertNotEquals(entityRepository.get("entity").getGroups(), user1.getGroups());

    }

}
