package com.lineate.traineeship;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
@Ignore
public class MockitoTest {

    @Test
    public void testTest() {
        EntityRepository repository = Mockito.mock(EntityRepository.class);
        EntityService entityService =
                new ServiceFactory().createEntityService(repository);

        User user = new User("", new Group("", null));

        Entity entity = new Entity(user, "e1");
        entity.setValue("");

        entityService.createEntity(user, "e1", "");
        entityService.createEntity(user, "e1", "");

        Mockito.verify(repository, Mockito.times(2)).save(entity);
    }
}
