package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

public class GroupNameTest {

    //Тут имя можно любое делать, даже очень плохое


    @Test
    public void testGroupName1(){
        Group group = new UserService().createGroup("", Collections.singleton(Permission.write));
        Assert.assertEquals("", group.getName());
    }

    @Test
    public void testGroupName2(){
        Group group = new UserService().createGroup(null, Collections.singleton(Permission.write));
        Assert.assertNull( group.getName());
    }

}
