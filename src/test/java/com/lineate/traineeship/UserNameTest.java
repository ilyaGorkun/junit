package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Collections;

public class UserNameTest {

    //Может быть пустая, а при null кидает NPE

    @Test
    public void testNameUser1() {

        UserService userService = new UserService();

        Group group = userService.createGroup("g1", Collections.singleton(Permission.write));

        User user = userService.createUser("", group);

        Assert.assertEquals("", user.getName());

    }

    @Ignore
    public void testNameUser2() {

        UserService userService = new UserService();

        Group group = userService.createGroup("g1", Collections.singleton(Permission.write));

        User user = userService.createUser(null, group);

        Assert.assertNull(user.getName());

    }

}
