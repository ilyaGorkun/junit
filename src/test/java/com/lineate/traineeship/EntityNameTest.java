package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collections;

public class EntityNameTest {
    @Ignore
    public void testEntityContainsSpace1(){
        UserService userService = new ServiceFactory().createUserService();

        EntityService entityService = new ServiceFactory().createEntityService();

        Group group = userService.createGroup("group", Collections.singleton(Permission.read));

        User user = userService.createUser("", group);

        Assert.assertFalse(entityService.createEntity(user, "Hello World",""));
    }


    @Ignore
    public void testEntityContainsSpace2(){
        UserService userService = new ServiceFactory().createUserService();

        EntityService entityService = new ServiceFactory().createEntityService();

        Group group = userService.createGroup("", Collections.singleton(Permission.read));

        User user = userService.createUser("Bob", group);

        Assert.assertTrue(entityService.createEntity(user, "HelloWorld",""));
    }


    @Ignore
    public void testEntityNameIsNotEmpty() {
        UserService userService = new ServiceFactory().createUserService();

        EntityService entityService = new ServiceFactory().createEntityService();

        Group group = userService.createGroup("", Collections.singleton(Permission.read));

        User user = userService.createUser("Bob", group);

        Assert.assertFalse(entityService.createEntity(user, " ",""));
    }

    @Ignore
    public void testEntityNameLessAn32() {
        UserService userService = new ServiceFactory().createUserService();

        EntityService entityService = new ServiceFactory().createEntityService();

        Group group = userService.createGroup("", Collections.singleton(Permission.read));

        User user = userService.createUser("Bob", group);

        Assert.assertFalse(entityService.createEntity(user, "uhwpuhweiuwehpiuwegewpufhweufhwepiufhweaiupfhwaiuepfhweiupfhwiuef",""));
    }

    //Это я баловался с парамметризированными тестами
    @Parameterized.Parameters(name = "{index}: [{0}] -> {1}")
    public static Iterable<Object[]> names() {
        return Arrays.asList(new Object[][] {
                {"Hello World", false},
                {"HelloWorld", true},
                {" ", false},
                {"uhwpuhweiuwehpiuwegewpufhweufhwepiufhweaiupfhwaiuepfhweiupfhwiuef", false}
        });
    }

    @Parameterized.Parameter(0)
    public String fInput;

    @Parameterized.Parameter(1)
    public boolean fExpected;

    @Test
    public void testName(){

        UserService userService = new ServiceFactory().createUserService();

        EntityService entityService = new ServiceFactory().createEntityService();

        Group group = userService.createGroup("group", Collections.singleton(Permission.read));

        User user = userService.createUser("user", group);

        Assert.assertEquals(fExpected, entityService.createEntity(user, fInput, "value"));


    }




}
